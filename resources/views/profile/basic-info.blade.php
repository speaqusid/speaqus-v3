<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5">
  <div class="profile-picture">
    @if($role == 1)
    <img src="{{ url('images/users/boto_simatupang.jpeg') }}" width="100%">
    @endif
    @if($role == 2)
    <img src="{{ url('images/groups/supercoach.jpg') }}" width="100%">
    @endif
    <a class="btn full-width">Edit Basic Profile</a>
  </div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 user-list-info" style="padding-right:30px;">
  <div class="row pointer" title="Verified User">
    @if($role == 1)
    <a class="user-name">Mulyono Sephiaques</a>
    @endif
    @if($role == 2)
    <a class="user-name">Super Cool Coach</a>
    @endif
    <i class="fa fa-check-circle text-green bigger-1-5 pointer"></i>
    <span class="user-score bigger-1-5">9.0</span>
  </div>
  <div class="row">
    <i class="fa fa-map"></i>
    Jakarta, Indonesia
    <i class="fa fa-comment"></i>
    Indonesian, English
  </div>
  <div class="row">
    <p>Entrepreneur, Business Lecturer at Smartlearn University, Vice Rector of Employability
      and Entrepreneurship Center, Business Speaker</p>
  </div>
  <div class="row">
    <b>Expert on:</b><br/>
    <a class="skill-tag tag" title="10 persons endorsed this skill">Entrepreneurship <span class="bold">10</span></a>
    <a class="skill-tag tag" title="10 persons endorsed this skill">Business <span class="bold">10</span></a>
    <a class="skill-tag tag" title="10 persons endorsed this skill">Leadership <span class="bold">10</span></a>
    <a class="skill-tag tag" title="10 persons endorsed this skill">Key Performance Indicator <span class="bold">10</span></a>
    <a class="skill-tag tag" title="10 persons endorsed this skill">Organizational Development <span class="bold">10</span></a>
    <a class="skill-tag tag" title="10 persons endorsed this skill">Soft Skill <span class="bold">10</span></a>
    <a class="skill-tag tag" title="10 persons endorsed this skill">Communication <span class="bold">10</span></a>
    <a class="skill-tag tag">more <i class="fa fa-angle-right"></i></a>
  </div>
  <div class="row">
    <b>Experienced In:</b><br/>
    <a class="industry-tag tag" title="10 persons verified this">Franchise <span class="bold">10</span></a>
    <a class="industry-tag tag" title="10 persons verified this">Academic <span class="bold">10</span></a>
    <a class="industry-tag tag" title="10 persons verified this">Information Technology <span class="bold">10</span></a>
    <a class="skill-tag tag">more <i class="fa fa-angle-right"></i></a>
  </div>
  <div class="row">
    <b>Audiences:</b><br/>
    <a class="audience-tag tag" title="10 persons verified this">Chief X Officer <span class="bold">10</span></a>
    <a class="audience-tag tag" title="10 persons verified this">Manager <span class="bold">10</span></a>
  </div>
  <br/>
</div>
