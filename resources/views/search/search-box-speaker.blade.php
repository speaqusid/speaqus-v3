<div class="row">
  <div class="col-lg-12">
      <h5 class="text-center border-bottom">Search for <span class="bold">SPEAKERS</span></h5>
  </div>
  <form>
    <div class="col-lg-12">
      <label>
        <span>Keywords</span>
        <input type="text" class="form-control" placeholder="Search.." />
      </label>
    </div>
    <div class="col-lg-12">
      <label>
        <span>Topic</span>
        <select class="form-control">
          <option>-- Any --</option>
          <option>Retail</option>
          <option>Engineering</option>
          <option>Business</option>
          <option>Psychology</option>
        </select>
      </label>
    </div>
    <div class="col-lg-12">
      <label>
        <span>Location</span>
        <input type="text" class="form-control" placeholder="Search.." />
      </label>
    </div>
    <div class="col-lg-12">
      <label>
        <span>Budget</span>
        <input type="text" class="form-control" placeholder="Minimum Budget" />
      </label>
    </div>
    <div class="col-lg-12">
      <label>
        <span></span>
        <input type="text" class="form-control" placeholder="Maximum Budget" />
      </label>
    </div>
    <div class="col-lg-12">
      <a href="#" class="btn">
        <span class="fa fa-search"></span>
        <b>SEARCH</b>
      </a>
    </div>
  </form>
  <div class="col-lg-12">
    <br/>
    <a href="#">Looking for Public Training instead?</a>
  </div>
</div>
