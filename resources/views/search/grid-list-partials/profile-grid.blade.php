<div class="col-lg-12 box-grid">
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
    <div class="profile-picture">
      @if($role == 1)
      <img src="{{ url('images/users/boto_simatupang.jpeg') }}" width="100%">
      @endif
      @if($role == 2)
      <img src="{{ url('images/groups/supercoach.jpg') }}" width="100%">
      @endif
      <a href="#" class="btn btn-default full-width">
        <i class="fa fa-mail-forward"></i>
        Profile
      </a>
    </div>
  </div>
  <div class="col-lg-6 col-md-7 col-sm-7 col-xs-6 user-list-info">
    <div class="row pointer" title="Verified User">
      @if($role == 1)
      <a href="{{url('/u/boto-simatupang')}}" class="user-name">Mulyono Sephiaques</a>
      @endif
      @if($role == 2)
      <a href="{{url('/g/super-cool-coach')}}" class="user-name">Super Cool Coach</a>
      @endif
      <i class="fa fa-check-circle text-green bigger-1-5 pointer"></i>
      <span class="user-score">9.0</span>
    </div>
    <div class="row">
      <i class="fa fa-map"></i>
      Jakarta, Indonesia
      <i class="fa fa-comment"></i>
      Indonesian, English
    </div>
    <div class="row">
      <p>Entrepreneur, Business Lecturer at Smartlearn University, Vice Rector of Employability
        and Entrepreneurship Center, Business Speaker</p>
    </div>
    <div class="row">
      <a class="skill-tag tag" title="10 persons endorsed this skill">Entrepreneurship <span class="bold">10</span></a>
      <a class="skill-tag tag" title="10 persons endorsed this skill">Business <span class="bold">10</span></a>
      <a class="skill-tag tag" title="10 persons endorsed this skill">Leadership <span class="bold">10</span></a>
      <a class="skill-tag tag" title="10 persons endorsed this skill">Key Performance Indicator <span class="bold">10</span></a>
      <a class="skill-tag tag" title="10 persons endorsed this skill">Organizational Development <span class="bold">10</span></a>
      <a class="skill-tag tag" title="10 persons endorsed this skill">Soft Skill <span class="bold">10</span></a>
      <a class="skill-tag tag" title="10 persons endorsed this skill">Communication <span class="bold">10</span></a>
      <a class="skill-tag tag">more <i class="fa fa-angle-right"></i></a>
    </div>
    <br/>
  </div>
  <div class="col-lg-4 col-md-3 col-sm-3 col-xs-3 user-side-info">
    <div class="row">
      <div class="col-lg-12">
        <a class="btn full-width trigger-popup trigger-sign-in">
          <i class="fa fa-plus"></i>
          Connect
        </a>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <b>Experienced In:</b><br/>
        <a class="industry-tag tag" title="10 persons verified this">Franchise <span class="bold">10</span></a>
        <a class="industry-tag tag" title="10 persons verified this">Academic <span class="bold">10</span></a>
        <a class="industry-tag tag" title="10 persons verified this">Information Technology <span class="bold">10</span></a>
        <a class="skill-tag tag">more <i class="fa fa-angle-right"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <b>Audiences:</b><br/>
        <a class="audience-tag tag" title="10 persons verified this">Chief X Officer <span class="bold">10</span></a>
        <a class="audience-tag tag" title="10 persons verified this">Manager <span class="bold">10</span></a>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <i class="fa fa-thumb-up"></i>
        <b>30</b> Connections
      </div>
      <div class="col-lg-6">
        <i class="fa fa-thumb-up"></i>
        <b>30</b> Reviews
      </div>
      <div class="col-lg-6">
        <i class="fa fa-thumb-up"></i>
        <b>30</b> Trainings
      </div>
      <div class="col-lg-6">
        <i class="fa fa-thumb-up"></i>
        <b>3000</b> views
      </div>
      <!--
      <div class="col-lg-12">
        <i class="fa fa-thumb-up"></i>
        <b>3</b> Years Training Experience
      </div>
      -->
    </div>
    <!--
    <div class="row">
      <div class="col-lg-12">
        Price Range <br/>
        <b>Rp 15.000.000 </b>
        -
        <b>Rp 30.000.000 </b>
      </div>
    </div>
    -->
  </div>
  <!-- Training Info! -->
  <!--
  <div class="col-lg-12" style="border-top:1px solid rgba(0,0,0, .1); position:absolute; bottom:0; height:auto; overflow:hidden; padding:10px; z-index:+1; background:#f5f5f5">
    <i class="fa fa-bell"></i>
    Will be speaking at
    <a href="#">Great Business Comes from Finding the Right Opportunities</a>
    at Jakarta on March 23th 2016.
    <a class="#">Join Now</a>
  </div-->
</div>
