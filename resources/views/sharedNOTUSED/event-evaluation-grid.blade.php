<div class="col-lg-12 box-grid evaluation-grid padding-20">
  <div class="col-lg-12 col-md-7 col-sm-7 col-xs-6 training-list-info">
    <div class="row">
      <a class="user-name" style="font-size:0.8em">Entrepreneur Midset for the Future</a>
      <span class="user-score">9.0</span>
    </div>
    <div class="row">
      <i class="fa fa-map"></i>
      Jakarta, Indonesia
      <i class="fa fa-comment"></i>
      Indonesian, English
    </div>
    <div class="row">
      <p>A public training with a discussion about getting successful in business
      using the right way in order to search for opportunities.</p>
    </div>
    <div class="row">
      <a href="#" class="btn toggle-evaluation-expand" data-expand="0"><i class="fa fa-caret-right"></i> 3 Audience Evaluation</a>
    </div>
    <div class="row audience-evaluation-list" style="padding-left:20px; display:none;">
      @include('shared.audience-evaluations.each-evaluation')
    </div>
    <br/>
  </div>
</div>


<!-- dup 1 -->

<div class="col-lg-12 box-grid evaluation-grid padding-20">
  <div class="col-lg-12 col-md-7 col-sm-7 col-xs-6 training-list-info">
    <div class="row">
      <a class="user-name">Great Business Comes from Finding the Right Opportunities</a>
      <span class="user-score">9.0</span>
    </div>
    <div class="row">
      <i class="fa fa-map"></i>
      Jakarta, Indonesia
      <i class="fa fa-comment"></i>
      Indonesian, English
    </div>
    <div class="row">
      <p>A public training with a discussion about getting successful in business
      using the right way in order to search for opportunities.</p>
    </div>
    <div class="row">
      <a href="#" class="btn toggle-evaluation-expand" data-expand="0"><i class="fa fa-caret-right"></i> 3 Audience Evaluation</a>
    </div>
    <div class="row audience-evaluation-list" style="padding-left:20px; display:none;">
      @include('shared.audience-evaluations.each-evaluation')
    </div>
    <br/>
  </div>
</div>

<!-- end of dup 1 -->

<!-- dup 1 -->

<div class="col-lg-12 box-grid evaluation-grid padding-20">
  <div class="col-lg-12 col-md-7 col-sm-7 col-xs-6 training-list-info">
    <div class="row">
      <a class="user-name">Brilliant Mind, Brilliant Strategies</a>
      <span class="user-score">9.0</span>
    </div>
    <div class="row">
      <i class="fa fa-map"></i>
      Jakarta, Indonesia
      <i class="fa fa-comment"></i>
      Indonesian, English
    </div>
    <div class="row">
      <p>A public training with a discussion about getting successful in business
      using the right way in order to search for opportunities.</p>
    </div>
    <div class="row">
      <a href="#" class="btn toggle-evaluation-expand" data-expand="0"><i class="fa fa-caret-right"></i> 3 Audience Evaluation</a>
    </div>
    <div class="row audience-evaluation-list" style="padding-left:20px; display:none;">
      @include('shared.audience-evaluations.each-evaluation')
    </div>
    <br/>
  </div>
</div>

<!-- end of dup 1 -->
