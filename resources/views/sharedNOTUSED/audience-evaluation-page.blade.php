@extends('main-app')

@section('title', 'SPEAQUS')

@section('content')
  <!--
  <div class="row heading">
    <div class="col-lg-12" style="background:rgba(0,0,0, .6); padding:130px 0px 0 0px;">
      <div class="col-lg-6 col-md-offset-1">
      </div>
    </div>
  </div>
-->
  <br/><br/><br/><br/>
  <div class="container">
    <div class="row">
      <div class="col-md-3 sidebar">
        @include('shared.audience-evaluations.filter-box')
      </div>
      <div class="col-md-9 box-profile padding-20">

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="profile-tab-list">
              <li class="active" data-trigger="speaking-experiences">Evaluation Summary</li>
              <li data-trigger="audience-evaluation">Audience Evaluations</li>
              <li class="pull-right green-back text-white bold uppercase" data-trigger="work-experiences">
                <i class="fa fa-plus"></i>
                Add New Training
              </li>
            </ul>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-20 profile-section" data-section="evaluation-summary">
            <div class="box-grid col-lg-12">
              <div id="canvas-holder" class="padding-20 col-lg-3">
                <canvas id="chart-area" width="150" height="150"/>
              </div>
              <div class="col-lg-6">
                <div class="border-left padding-20">
                  <h4>Audience Seniority Level Statistics</h4>
                  <br/>
                  <p>
                    <i class="fa fa-stop text-green"></i>
                    <span class="bold">50</span> Chief X Officers
                  </p>
                  <p>
                    <i class="fa fa-stop text-blue"></i>
                    <span class="bold">30</span> Managers
                  </p>
                  <p>
                    <i class="fa fa-stop text-red"></i>
                    <span class="bold">10</span> Staffs
                  </p>
                </div>
              </div>
            </div>

            <script>
          		var pieData = [
          				{
          					value: 300,
          					color:"#F7464A",
          					highlight: "#FF5A5E",
          					label: "Red"
          				},
          				{
          					value: 50,
          					color: "#46BFBD",
          					highlight: "#5AD3D1",
          					label: "Green"
          				},
          				{
          					value: 100,
          					color: "#FDB45C",
          					highlight: "#FFC870",
          					label: "Yellow"
          				}
          			];
          			window.onload = function(){
          				var ctx = document.getElementById("chart-area").getContext("2d");
          				window.myPie = new Chart(ctx).Pie(pieData);
          			};
          	</script>

          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile-section" data-section="audience-evaluation">
            @include('shared.event-evaluation-grid')
            @include('shared.event-evaluation-grid')
            @include('shared.event-evaluation-grid')
          </div>
        </div>

        <div class="row"><br/><br/></div>
        <div class="row" style="margin-top:100px;">
          <center>
            <i class="fa fa-circle-o-notch fa-spin bigger-2 blue-border circle text-blue" style="padding:15px;"></i>
          </center>
        </div>
        <div class="row"><br/><br/><br/></div>
      </div>

    </div>

  </div>

@stop
