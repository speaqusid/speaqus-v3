@extends('main-app')

@section('title', 'SPEAQUS')

@section('content')
  <br/><br/><br/><br/>
  <div class="container">
    <a href="{{url('super-cool-coach/trainings')}}" class="btn"><i class="fa fa-angle-left"></i> Go back</a>
    <div class="row box-profile">
      <div class="col-md-12 box-section profile-section datatable-section" data-section="trainings">
        <div class="row text-center">
          <h3><span class="lnr lnr-magnifier bigger-1-5 blue-border circle text-blue" style="padding:15px;"></span></h3>
          <br/>
          <h3 class="roboto-light text-blue">TRAINING: LEADERSHIP AND ENTREPREURSHIP DEVELOPMENT</h3>
        </div>
        <br/>
        <table id="audience-table" class="display table" width="100%" cellspacing="0">
          <thead>
            <tr class="uppercase">
              <th>No.</th>
              <th>Audience Name</th>
              <th>Job Title</th>
              <th>Job Level</th>
              <th>Job Function</th>
              <th>Material</th>
              <th>Material Description</th>
              <th>Delivery</th>
              <th>Delivery Description</th>
              <th>Facility</th>
              <th>Facility Description</th>
              <th>Post Test Score</th>
            </tr>
          </thead>
          <tbody>
            @for($i=1;$i<15;$i++)
            <tr>
              <td>{{ $i }}</td>
              <td><a href="#">Jonathan Toby</a></td>
              <td>CEO</td>
              <td>CXO</td>
              <td>Board of Director</td>
              <td>9</td>
              <td>Bagus</td>
              <td>8</td>
              <td>Good</td>
              <td>6</td>
              <td>Awesome</td>
              <td>9.5</td>
            </tr>
            @endfor
          </tbody>
        </table>
      </div>

    </div>

  </div>

@stop
