<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'UserController@getUsers');

Route::get('/landing', 'WelcomeController@index');
Route::get('home', 'HomeController@index');

/*
|--------
| Search
|--------
*/

Route::get('/trainers', 'UserController@getUsers');
Route::get('/training-providers', 'GroupController@getGroups');

/*
|--------
| Users
|--------
*/
Route::get('/users', 'UserController@getUsers');
Route::get('/u/{slug}', 'UserController@getUser');
Route::get('/g/{slug}', 'GroupController@getGroup');
Route::get('/evaluation/{slug}', 'GeneralController@getEvaluation');

/*
|--------
| Business
|--------
*/
Route::get('{group}/trainings','TrainingController@getTrainings');
Route::get('{group}/training/{name}','TrainingController@getTrainingDetail');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
